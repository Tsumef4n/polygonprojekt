/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polygonprojekt;

import java.awt.*;
import javax.swing.*;

/**
 * @author lippel
 */
public class GUI extends JPanel {

    private static JFrame frame;
    private Polygon[] polygonListe; // Liste mit Polygonen oder daraus abgeliteten Objekten
    private int anzahlPolygone; // Aktuelle Anzahl Polygone in der Liste

    public GUI() {// Konstruktor
        polygonListe = new Polygon[10]; // Maximal 10 Polygone können verwendet werden
        anzahlPolygone = 0; // Aktuelle Anzahl der Polygone
        frame = new JFrame("Zeichenblock"); // Neues Fenster mit Fenstertitel
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);// damit das Fenster geschlossen wird, wenn man auf "Schließen" klickt
        frame.setBackground(Color.white); // Hintergrundfarbe weiß
        frame.setSize(800, 600); // Fenstergröße in Pixeln
        frame.setLocationRelativeTo(null); // in der Mitte vom Bildschirm platzieren
        frame.setVisible(true); // sichtbar machen
        frame.add(this); // Hokuspokus
    }
// Fügt der Liste von Polygonen ein neues hinzu, und zeichnet die Grafik neu

    public void fuegePolygonHinzu(Polygon poly) {
        if (anzahlPolygone >= polygonListe.length - 1) {
            System.out.println("Höchstzahl von " + polygonListe.length + " Polygonen ist erreicht!\nHinzufügen nicht möglich");
            return;
        }
        this.polygonListe[anzahlPolygone] = poly;
        anzahlPolygone++;
        System.out.println("zur GUI hinzugefügt: " + poly);
        repaint(); // zeichnet alles neu
    }
// Geerbte Methode, Zeichnet die Polygone. Wird u.a. bei repaint aufgerufen

    @Override
    public void paint(Graphics g) {
        Graphics2D g2d = (Graphics2D) g;
        g2d.setRenderingHint(RenderingHints.KEY_ANTIALIASING, RenderingHints.VALUE_ANTIALIAS_ON);
//improving the quality
        zeichneKoordinatensystem(g2d); // Gitternetzlinien einzeichnen
// Alle Polygone zeichnen
        for (int i = 0; i < anzahlPolygone; i++) {
            g2d.setColor(new Color(180, 80, 50, 180)); // Farbe auf halbtransparentes rot setzten
            g2d.fillPolygon(polygonListe[i]); // Polygon Zeichnen
            g2d.setColor(Color.red); // Farbe auf rot setzten
// Startpunkt des Polygons zeichnen
            g2d.fillOval(polygonListe[i].xpoints[0] - 3, polygonListe[i].ypoints[0] - 3, 7, 7);
        }
    }
// Zeichnet ein Koordinatensystem und Gitternetzlinien

    private void zeichneKoordinatensystem(Graphics2D g2d) {
        Color before = g2d.getColor(); // aktuelle Farbe merken
        g2d.setColor(Color.black); // Farbe von Text und Grid
        int delta = 50; // Rasterpunkte
// Vertikale Linien und Beschriftung
        for (int i = delta; i < getWidth(); i += delta) {
            g2d.drawLine(i, delta / 2, i, getHeight());
            g2d.drawString("" + i, i - 10, 15);
        } // Horizontale Linien
        for (int i = delta; i < getHeight(); i += delta) {
            g2d.drawLine(delta / 2, i, getWidth(), i);
            g2d.drawString("" + i, 3, i + 5);
        }
// vorherige Farbe wiederherstellen
        g2d.setColor(before);
    }
}
