/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polygonprojekt;

/**
 *
 * @author KeStueve
 */
public class Hauptprogramm {

    /**
     * @param args the command line arguments
     */
    public static void main(String[] args) {
        GUI g = new GUI();
        
        // Obere reihe
        Isogon i1 = new Isogon(100, 100, 50, 3);
        g.fuegePolygonHinzu(i1);
        Isogon i2 = new Isogon(200, 100, 50, 3, 0);
        g.fuegePolygonHinzu(i2);
        Isogon i3 = new Isogon(300, 100, 50, 3, 30);
        g.fuegePolygonHinzu(i3);
        // Mittlere Reihe
        Quadrat i4 = new Quadrat(100, 250, 100, 45);
        g.fuegePolygonHinzu(i4);
        Kreis i5 = new Kreis(200, 250, 40);
        g.fuegePolygonHinzu(i5);
        Isogon i6 = new Isogon(300, 250, 50, 8, -90);
        g.fuegePolygonHinzu(i6);
        // untere Reihe
        Quadrat i7 = new Quadrat(100, 400, 50, 4);
        g.fuegePolygonHinzu(i7);
        Isogon i8 = new Isogon(200, 400, 50, 5, 270);
        g.fuegePolygonHinzu(i8);
        Isogon i9 = new Isogon(350, 400, 80, 6, -90);
        g.fuegePolygonHinzu(i9);
    }
    
}
