/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polygonprojekt;

import java.awt.Polygon;
import java.awt.Point;

/**
 *
 * @author KeStueve
 */
public class Isogon extends Polygon {
    
    private Point mitte;
    private int startwinkel = 0;
    private int radius = 0;
    private int anzahlEcken = 0; //extra?
    
    public Isogon(int mitteX, int mitteY, int radius, int anzahlEcken)
    {
        this.mitte = new Point(mitteX, mitteY);
        this.radius = radius;
        setEckenzahl(anzahlEcken);
        zeichnePolygon();
    }
    
    public Isogon(int mitteX, int mitteY, int radius, int anzahlEcken, int startwinkel)
    {
        this.mitte = new Point(mitteX, mitteY);
        this.radius = radius;
        this.startwinkel = startwinkel;
        setEckenzahl(anzahlEcken);
        zeichnePolygon();
    }

    
    public void setEckenzahl(int anzahlEcken)
    {
        this.anzahlEcken = anzahlEcken;
        zeichnePolygon();
    }
    
    public void translate(int deltaX, int deltaY)
    {
        super.translate(deltaX, deltaY);
        zeichnePolygon();
    }
    
    public void rotiere(int winkel)
    {
        this.startwinkel += winkel;
        zeichnePolygon();
    }
    
    public Isogon kopiere()
    {
        Isogon test = new Isogon(this.mitte.x, this.mitte.y, this.radius, this.anzahlEcken, this.startwinkel);
        return test;
    }
    
    public void verschiebeAufNeueMitte(int mitteX, int mitteY)
    {
        this.mitte = new Point(mitteX, mitteY);
    }
    
    @Override
    public void addPoint(int x, int y)
    {
        System.out.print("Lokale Methode wird nicht benutzt!");
    }
    
    @Override
    public String toString()
    {
        System.out.print("Hinzugefuegt: ");
        switch(this.anzahlEcken)
        {
            case 3:
                System.out.print("Dreieck: ");
                break;
            case 4:
                System.out.print("Quadrat: ");
                break;
            case 5:
                System.out.print("Pentagon: ");
                break;
            case 6:
                System.out.print("Hexagon: ");
                break;
            case 7:
                System.out.print("Heptagon: ");
                break;
            case 8:
                System.out.print("Oktagon: ");
                break;
            default:
                System.out.print(this.anzahlEcken + "-Eck: ");
                break;
        }
        System.out.print("X-Eck: ");
        for (int i=0;i<super.npoints;i++){
        System.out.print("P"+i+ "("+super.xpoints[i]+", "+super.xpoints[i]+") ");
        }
        return "";
    }

    public void zeichnePolygon()
    {
        this.reset();
        for (int i = 0; i < anzahlEcken; i++) 
        {
            double dx = (this.radius * Math.cos(Math.toRadians(this.startwinkel) + ((2 * Math.PI * i) / this.anzahlEcken))) + mitte.x;
            double dy = (this.radius * Math.sin(Math.toRadians(this.startwinkel) + ((2 * Math.PI * i) / this.anzahlEcken))) + mitte.y;
            int x = (int) Math.round(dx);
            int y = (int) Math.round(dy);
            //System.out.print("P"+i+ " = ("+x+", "+y+")");
            super.addPoint((int)x, (int)y);
        }
    }

}
