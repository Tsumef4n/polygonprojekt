/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polygonprojekt;

/**
 *
 * @author KeStueve
 */
public class Pentagon extends Isogon {
    public Pentagon(int mitteX, int mitteY, int radius, int winkel)
    {
        super(mitteX, mitteY, radius, 5, winkel);
    }
    
    public Pentagon(int mitteX, int mitteY, int radius)
    {
        super(mitteX, mitteY, radius, 5);
    }
}
