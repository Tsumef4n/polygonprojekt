/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package polygonprojekt;

/**
 *
 * @author KeStueve
 */
public class Quadrat extends Isogon{
    
    public Quadrat(int mitteX, int mitteY, int seitenlaenge, int startwinkel) {
        super(mitteX, mitteY, (int)Math.round(seitenlaenge / Math.sqrt(2)), 4, startwinkel);
    }
    
    public Quadrat(int mitteX, int mitteY, int seitenlaenge) {
        super(mitteX, mitteY, (int)Math.round(seitenlaenge / Math.sqrt(2)), 4);
    }
    
}
